package com.jth.wematch.repository;

import com.jth.wematch.entity.Complain;
import com.jth.wematch.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComplainRepository extends JpaRepository<Complain, Long> {
    Page<Complain> findAllByMember_IsEnabledOrderByIdAsc(boolean isEnabled, PageRequest pageRequest);
    Page<Complain> findAllByMember_IdOrderByIdAsc(long memberId, PageRequest pageRequest);
}
