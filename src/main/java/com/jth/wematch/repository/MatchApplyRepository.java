package com.jth.wematch.repository;

import com.jth.wematch.entity.MatchApply;
import com.jth.wematch.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MatchApplyRepository extends JpaRepository<MatchApply, Long> {
    Optional<MatchApply> findByMemberAndMatch_IdAndMatch_IsEnable(Member member, long matchId, Boolean isEnable);
    Page<MatchApply> findAllByMatch_IdAndMatch_IsEnable(long matchId, Boolean isEnable, PageRequest pageRequest);
    Page<MatchApply> findAllByMemberAndMatch_IsEnable(Member member, Boolean isEnable, PageRequest pageRequest);
}
