package com.jth.wematch.repository;

import com.jth.wematch.entity.ComplainAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComplainAnswerRepository extends JpaRepository<ComplainAnswer, Long> {
}
