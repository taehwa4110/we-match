package com.jth.wematch.repository;

import com.jth.wematch.entity.Match;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface MatchRepository extends JpaRepository<Match, Long> {
    List<Match> findAllByPlaceOrSportDateOrderByIdAsc(String place, LocalDate sportDate);
    List<Match> findAllByMember_Id(Long memberId);
    Match findAllByMember_IdAndId(Long memberId, Long matchId);
}
