package com.jth.wematch.entity;

import com.jth.wematch.enums.AcceptStatus;
import com.jth.wematch.interfaces.CommonModelBuilder;
import com.jth.wematch.lib.CommonDate;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MatchApply {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "matchId", nullable = false)
    private Match match;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private AcceptStatus acceptStatus;

    @Column(nullable = false)
    private Boolean isEnabled;

    private LocalDateTime dateAccept;

    private LocalDateTime dateRefuse;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putIsEnabled() {
        this.isEnabled = false;
        this.dateUpdate = CommonDate.getNowTime();
    }

    public void putAcceptStatus(AcceptStatus acceptStatus) {
        this.acceptStatus = acceptStatus;

        switch (acceptStatus) {
            case ACCEPT:
                this.dateAccept = CommonDate.getNowTime();
                break;
            case REFUSAL:
                this.dateRefuse = CommonDate.getNowTime();
                break;
        }
    }


    private MatchApply(MatchApplyBuilder builder) {
        this.member = builder.member;
        this.match = builder.match;
        this.acceptStatus = builder.acceptStatus;
        this.isEnabled = builder.isEnabled;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }


    public static class MatchApplyBuilder implements CommonModelBuilder<MatchApply> {
        private final Member member;
        private final Match match;
        private final AcceptStatus acceptStatus;
        private final Boolean isEnabled;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MatchApplyBuilder(Member member, Match match) {
            this.member = member;
            this.match = match;
            this.acceptStatus = AcceptStatus.WAITING;
            this.isEnabled = true;
            this.dateCreate = CommonDate.getNowTime();
            this.dateUpdate = CommonDate.getNowTime();
        }

        @Override
        public MatchApply build() {
            return new MatchApply(this);
        }
    }

}
