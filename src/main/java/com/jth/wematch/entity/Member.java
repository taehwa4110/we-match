package com.jth.wematch.entity;

import com.jth.wematch.enums.Gender;
import com.jth.wematch.enums.MemberGroup;
import com.jth.wematch.interfaces.CommonModelBuilder;
import com.jth.wematch.lib.CommonDate;
import com.jth.wematch.model.member.MemberInfoUpdateRequest;
import com.jth.wematch.model.member.MemberJoinRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //시퀀스

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private MemberGroup memberGroup; //권한

    @Column(nullable = false, unique = true , length = 30)
    private String username; //아이디

    @Column(nullable = false)
    private String password; //패스워드는 중요한 정보이니까 이제 앞으로 암호화할것이기에 길이는 255자로 사용

    @Column(nullable = false, length = 20)
    private String name; //이름

    @Column(nullable = false)
    private LocalDate birthday; //생년월일

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Gender gender; //성별

    @Column(nullable = false, length = 20)
    private String phone; //전화번호

    private String sportsType; // 주 종목

    private String location; //활동지

    private String imageName; //이미지 이름

    @Column(nullable = false)
    private Boolean isEnabled; //활성화 여부

    @Column(nullable = false)
    private LocalDateTime dateCreate; //등록일

    @Column(nullable = false)
    private LocalDateTime dateUpdate; //수정일


    private LocalDateTime dateUploadImage; //이미지 업로드일

    private LocalDateTime dateDisable; //탈퇴일


    public void putMemberInfo(MemberInfoUpdateRequest request) {
        this.name = request.getName();
        this.phone = request.getPhone();
        this.dateUpdate = CommonDate.getNowTime();
    }

    public void putIsEnabled() {
        this.isEnabled = false;
        this.dateDisable = CommonDate.getNowTime();
    }

    private Member(MemberBuilder builder) {
        this.memberGroup = builder.memberGroup;
        this.username = builder.username;
        this.password = builder.password;
        this.name = builder.name;
        this.birthday = builder.birthday;
        this.gender = builder.gender;
        this.phone = builder.phone;
        this.isEnabled = builder.isEnabled;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final MemberGroup memberGroup;
        private final String username;
        private final String password;
        private final String name;
        private final LocalDate birthday;
        private final Gender gender;
        private final String phone;
        private final Boolean isEnabled;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MemberBuilder(MemberGroup memberGroup, MemberJoinRequest request) {
            this.memberGroup = memberGroup;
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.name = request.getName();
            this.birthday = request.getBirthday();
            this.gender = request.getGender();
            this.phone = request.getPhone();
            this.isEnabled = true;
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() { //어떤권한을 제공 할것인지
        return Collections.singleton(new SimpleGrantedAuthority(memberGroup.toString()));
    }

    @Override
    public boolean isAccountNonExpired() { //가입에 대한 만료일이 유효하니?
        return true;
    }

    @Override
    public boolean isAccountNonLocked() { // 안 잠겻지?
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() { //자격증명이 유효하니?
        return true;
    }

    @Override
    public boolean isEnabled() { //활성화되어있니??
        return this.isEnabled;
    }
}
