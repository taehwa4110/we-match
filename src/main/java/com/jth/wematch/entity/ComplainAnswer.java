package com.jth.wematch.entity;

import com.jth.wematch.interfaces.CommonModelBuilder;
import com.jth.wematch.lib.CommonDate;
import com.jth.wematch.model.complain.ComplainAnswerRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplainAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "complainId", nullable = false)
    private Complain complain;

    @Column(nullable = false)
    private String contents;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    private ComplainAnswer(ComplainAnswerBuilder builder) {
        this.member = builder.member;
        this.complain = builder.complain;
        this.contents = builder.contents;
        this.dateCreate = builder.dateCreate;
    }

    public static class ComplainAnswerBuilder implements CommonModelBuilder<ComplainAnswer> {
        private final Member member;
        private final Complain complain;
        private final String contents;
        private final LocalDateTime dateCreate;

        public ComplainAnswerBuilder(Member member, Complain complain, ComplainAnswerRequest request) {
            this.member = member;
            this.complain = complain;
            this.contents = request.getContents();
            this.dateCreate = CommonDate.getNowTime();
        }

        @Override
        public ComplainAnswer build() {
            return new ComplainAnswer(this);
        }
    }
}
