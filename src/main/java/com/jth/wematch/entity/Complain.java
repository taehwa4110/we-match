package com.jth.wematch.entity;

import com.jth.wematch.enums.ProcessStatus;
import com.jth.wematch.interfaces.CommonModelBuilder;
import com.jth.wematch.lib.CommonDate;
import com.jth.wematch.model.complain.ComplainRegisterRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Complain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false, length = 30)
    private String title;

    @Column(nullable = false)
    private String contents;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private ProcessStatus processStatus;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private LocalDateTime dateFinish;

    public void putProcessStatus() {
        this.processStatus = ProcessStatus.ANSWER_COMPLETE;
        this.dateUpdate = CommonDate.getNowTime();
    }

    private Complain(ComplainBuilder builder) {
        this.member = builder.member;
        this.title = builder.title;
        this.contents = builder.contents;
        this.processStatus = builder.processStatus;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class ComplainBuilder implements CommonModelBuilder<Complain> {
        private final Member member;
        private final String title;
        private final String contents;
        private final ProcessStatus processStatus;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public ComplainBuilder(Member member, ComplainRegisterRequest request) {
            this.member = member;
            this.title = request.getTitle();
            this.contents = request.getContents();
            this.processStatus = ProcessStatus.ANSWER_WAITING;
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Complain build() {
            return new Complain(this);
        }
    }
}
