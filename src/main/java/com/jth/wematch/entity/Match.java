package com.jth.wematch.entity;

import com.jth.wematch.interfaces.CommonModelBuilder;
import com.jth.wematch.model.match.MatchInfoUpdateRequest;
import com.jth.wematch.model.match.MatchRegisterRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Match {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false, length = 30)
    private String place;

    @Column(nullable = false, length = 40)
    private String placeAddress;

    @Column(nullable = false, length = 30)
    private String title;

    @Column(nullable = false)
    private String contents;

    @Column(nullable = false)
    private LocalDate sportDate;

    @Column(nullable = false)
    private Double posX;

    @Column(nullable = false)
    private Double posY;

    @Column(nullable = false)
    private Integer peopleCount;

    @Column(nullable = false)
    private Boolean isEnable;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putMatchInfo(MatchInfoUpdateRequest request) {
        this.place = request.getPlace();
        this.placeAddress = request.getPlaceAddress();
        this.title = request.getTitle();
        this.contents = request.getContents();
        this.sportDate = request.getSportDate();
        this.dateUpdate = LocalDateTime.now();
    }

    public void putMatchEnable() {
        this.isEnable = false;
        this.dateUpdate = LocalDateTime.now();
    }

    private Match(MatchBuilder builder) {
        this.member = builder.member;
        this.place = builder.place;
        this.placeAddress = builder.placeAddress;
        this.title = builder.title;
        this.contents = builder.contents;
        this.sportDate = builder.sportDate;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.peopleCount = builder.peopleCount;
        this.isEnable = builder.isEnable;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MatchBuilder implements CommonModelBuilder<Match> {
        private final Member member;
        private final String place;
        private final String placeAddress;
        private final String title;
        private final String contents;
        private final LocalDate sportDate;
        private final Double posX;
        private final Double posY;
        private final Integer peopleCount;
        private final Boolean isEnable;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MatchBuilder(Member member, MatchRegisterRequest request) {
            this.member = member;
            this.place = request.getPlace();
            this.placeAddress = request.getPlaceAddress();
            this.title = request.getTitle();
            this.contents = request.getContents();
            this.sportDate = request.getSportDate();
            this.posX = request.getPosX();
            this.posY = request.getPosY();
            this.peopleCount = request.getPeopleCount();
            this.isEnable = true;
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Match build() {
            return new Match(this);
        }
    }
}
