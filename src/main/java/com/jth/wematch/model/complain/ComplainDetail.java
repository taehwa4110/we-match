package com.jth.wematch.model.complain;

import com.jth.wematch.entity.Complain;
import com.jth.wematch.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplainDetail {
    @ApiModelProperty(notes = "문의 제목")
    private String complainTitle;
    @ApiModelProperty(notes = "작성자")
    private String memberName;
    @ApiModelProperty(notes = "문의 내용")
    private String complainContents;
    @ApiModelProperty(notes = "작성일")
    private LocalDateTime dateCreate;

    private ComplainDetail(ComplainDetailBuilder builder) {
        this.complainTitle = builder.complainTitle;
        this.memberName = builder.memberName;
        this.complainContents = builder.complainContents;
        this.dateCreate = builder.dateCreate;
    }

    public static class ComplainDetailBuilder implements CommonModelBuilder<ComplainDetail> {
        private final String complainTitle;
        private final String memberName;
        private final String complainContents;
        private final LocalDateTime dateCreate;

        public ComplainDetailBuilder(Complain complain) {
            this.complainTitle = complain.getTitle();
            this.memberName = complain.getMember().getName();
            this.complainContents = complain.getContents();
            this.dateCreate = complain.getDateCreate();
        }

        @Override
        public ComplainDetail build() {
            return new ComplainDetail(this);
        }
    }
}
