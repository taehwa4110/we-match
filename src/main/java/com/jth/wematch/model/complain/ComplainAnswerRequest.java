package com.jth.wematch.model.complain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ComplainAnswerRequest {
    @ApiModelProperty(notes = "문의 답변 내용")
    @NotNull
    @Length(max = 255)
    private String contents;
}
