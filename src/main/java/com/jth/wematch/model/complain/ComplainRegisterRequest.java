package com.jth.wematch.model.complain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ComplainRegisterRequest {
    @ApiModelProperty(notes = "문의 제목 (2 ~ 30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String title;

    @ApiModelProperty(notes = "문의 내용 (~ 255자)", required = true)
    @NotNull
    @Length(max = 255)
    private String contents;
}
