package com.jth.wematch.model.complain;

import com.jth.wematch.entity.Complain;
import com.jth.wematch.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComplainListItem {
    private String complainTitle;
    private String processStatus;
    private LocalDateTime dateCreate;

    private ComplainListItem(ComplainListItemBuilder builder) {
        this.complainTitle = builder.complainTitle;
        this.processStatus = builder.processStatus;
        this.dateCreate = builder.dateCreate;
    }

    public static class ComplainListItemBuilder implements CommonModelBuilder<ComplainListItem> {
        private final String complainTitle;
        private final String processStatus;
        private final LocalDateTime dateCreate;

        public ComplainListItemBuilder(Complain complain) {
            this.complainTitle = complain.getTitle();
            this.processStatus = complain.getProcessStatus().getName();
            this.dateCreate = complain.getDateCreate();
        }


        @Override
        public ComplainListItem build() {
            return new ComplainListItem(this);
        }
    }


}
