package com.jth.wematch.model.member;

import com.jth.wematch.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberJoinRequest {
    @ApiModelProperty(notes = "아이디 (5 ~ 30자)", required = true)
    @NotNull
    @Length(min = 5, max = 30)
    private String username; //아이디

    @ApiModelProperty(notes = "비밀번호 (8 ~ 20자)", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String password; //패스워드

    @ApiModelProperty(notes = "비밀번호 재확인(8 ~ 20자)", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String passwordRe;

    @ApiModelProperty(notes = "이름 (2 ~ 20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name; //이름

    @ApiModelProperty(notes = "생년월일", required = true)
    @NotNull
    private LocalDate birthday; //생년월일

    @ApiModelProperty(notes = "성별", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender; //성별

    @ApiModelProperty(notes = "전화번호 (13 ~ 20자)", required = true)
    @NotNull
    @Length(min = 13, max = 20)
    private String phone; //전화번호
}
