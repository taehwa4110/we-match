package com.jth.wematch.model.member;

import com.jth.wematch.entity.Member;
import com.jth.wematch.enums.Gender;
import com.jth.wematch.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberListItem {
    private String name;
    private String gender;
    private String phone;
    private String isEnabled;

    private MemberListItem(MemberListItemBuilder builder) {
        this.name = builder.name;
        this.gender = builder.gender;
        this.phone = builder.phone;
        this.isEnabled = builder.isEnabled;
    }


    public static class MemberListItemBuilder implements CommonModelBuilder<MemberListItem> {
        private final String name;
        private final String gender;
        private final String phone;
        private final String isEnabled;

        public MemberListItemBuilder(Member member) {
            this.name = member.getName();
            this.gender = member.getGender().getName();
            this.phone = member.getPhone();
            this.isEnabled = member.getIsEnabled() ? "사용중" : "미사용중";
        }

        @Override
        public MemberListItem build() {
            return new MemberListItem(this);
        }
    }
}
