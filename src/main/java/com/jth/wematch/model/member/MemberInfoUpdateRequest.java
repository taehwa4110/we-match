package com.jth.wematch.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberInfoUpdateRequest {
    @ApiModelProperty(notes = "고객 이름 (2 ~ 20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(notes = "고객 전화번호 (13 ~ 13자)", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String phone;
}
