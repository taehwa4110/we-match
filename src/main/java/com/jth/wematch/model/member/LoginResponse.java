package com.jth.wematch.model.member;

import com.jth.wematch.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    @ApiModelProperty(notes = "토큰")
    @NotNull
    private String token;

    @ApiModelProperty(notes = "이름")
    @NotNull
    private String name;

    public LoginResponse(LoginResponseBuilder builder) {
        this.token = builder.token;
        this.name = builder.name;
    }

    public static class LoginResponseBuilder implements CommonModelBuilder<LoginResponse> {
        private final String token;
        private final String name;

        public LoginResponseBuilder(String token, String name) {
            this.token = token;
            this.name = name;
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
