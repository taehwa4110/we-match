package com.jth.wematch.model.member;

import com.jth.wematch.entity.Member;
import com.jth.wematch.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberMyInfoDetail {
    @ApiModelProperty(notes = "고객 시퀀스")
    @NotNull
    private Long id;
    @ApiModelProperty(notes = "회원 그룹")
    @NotNull
    private String memberGroup;
    @ApiModelProperty(notes = "이름")
    @NotNull
    private String name;
    @ApiModelProperty(notes = "성별")
    @NotNull
    private String gender;
    @ApiModelProperty(notes = "전화번호")
    @NotNull
    private String phone;
    @ApiModelProperty(notes = "주종목")
    @NotNull
    private String sportsType;
    @ApiModelProperty(notes = "활동지")
    @NotNull
    private String location;
    @ApiModelProperty(notes = "프로필사진")
    @NotNull
    private String imageName;

    private MemberMyInfoDetail(MemberMyInfoDetailBuilder builder) {
        this.id = builder.id;
        this.memberGroup = builder.memberGroup;
        this.name = builder.name;
        this.gender = builder.gender;
        this.phone = builder.phone;
        this.sportsType = builder.sportsType;
        this.location = builder.location;
        this.imageName = builder.imageName;
    }

    public static class MemberMyInfoDetailBuilder implements CommonModelBuilder<MemberMyInfoDetail> {
        private final Long id;
        private final String memberGroup;
        private final String name;
        private final String gender;
        private final String phone;
        private final String sportsType;
        private final String location;
        private final String imageName;

        public MemberMyInfoDetailBuilder(Member member) {
            this.id = member.getId();
            this.memberGroup = member.getMemberGroup().getName();
            this.name = member.getName();
            this.gender = member.getGender().getName();
            this.phone = member.getPhone();
            this.sportsType = member.getSportsType();
            this.location = member.getLocation();
            this.imageName = member.getImageName();
        }

        @Override
        public MemberMyInfoDetail build() {
            return new MemberMyInfoDetail(this);
        }
    }
}
