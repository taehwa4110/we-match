package com.jth.wematch.model.match;

import com.jth.wematch.entity.Match;
import com.jth.wematch.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MatchListItem {
    @ApiModelProperty(notes = "경기 시퀀스")
    @NotNull
    private Long id;
    @ApiModelProperty(notes = "고객 이름")
    @NotNull
    private String memberName;
    @ApiModelProperty(notes = "게시글 제목")
    @NotNull
    private String fullTitle;
    @ApiModelProperty(notes = "게시글 내용")
    @NotNull
    private String contents;
    @ApiModelProperty(notes = "경기 날짜")
    @NotNull
    private LocalDate sportDate;
    @ApiModelProperty(notes = "현재 인원 수")
    @NotNull
    private Integer peopleCount;

    @ApiModelProperty(notes = "활성 여부")
    @NotNull
    private String isEnable;

    private MatchListItem(MatchListItemBuilder builder) {
        this.id = builder.id;
        this.memberName= builder.memberName;
        this.fullTitle = builder.fullTitle;
        this.contents = builder.contents;
        this.sportDate = builder.sportDate;
        this.peopleCount = builder.peopleCount;
        this.isEnable = builder.isEnable;
    }

    public static class MatchListItemBuilder implements CommonModelBuilder<MatchListItem> {
        private final Long id;
        private final String memberName;
        private final String fullTitle;
        private final String contents;
        private final LocalDate sportDate;
        private final Integer peopleCount;
        private final String isEnable;

        public MatchListItemBuilder(Match match) {
            this.id = match.getId();
            this.memberName = match.getMember().getName();
            this.fullTitle = "[" + match.getPlace() + "]" + match.getTitle();
            this.contents = match.getContents();
            this.sportDate = match.getSportDate();
            this.peopleCount = match.getPeopleCount();
            this.isEnable = match.getIsEnable() ? "모집중" : "기간 만료";
        }

        @Override
        public MatchListItem build() {
            return new MatchListItem(this);
        }
    }
}
