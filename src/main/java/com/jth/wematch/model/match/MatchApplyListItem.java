package com.jth.wematch.model.match;

import com.jth.wematch.entity.MatchApply;
import com.jth.wematch.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MatchApplyListItem {
    @ApiModelProperty(notes = "고객 이름")
    private String memberName;
    @ApiModelProperty(notes = "경기 수락 여부")
    private String acceptStatus;
    @ApiModelProperty(notes = "경기 신청일")
    private LocalDateTime matchApplyDate;



    private MatchApplyListItem(MatchApplyListItemBuilder builder) {
        this.memberName = builder.memberName;
        this.acceptStatus = builder.acceptStatus;
        this.matchApplyDate = builder.matchApplyDate;
    }

    public static class MatchApplyListItemBuilder implements CommonModelBuilder<MatchApplyListItem> {
        private final String memberName;
        private final String acceptStatus;
        private final LocalDateTime matchApplyDate;

        public MatchApplyListItemBuilder(MatchApply matchApply) {
            this.memberName = matchApply.getMember().getName();
            this.acceptStatus = matchApply.getAcceptStatus().getName();
            this.matchApplyDate = matchApply.getDateCreate();
        }


        @Override
        public MatchApplyListItem build() {
            return new MatchApplyListItem(this);
        }
    }
}
