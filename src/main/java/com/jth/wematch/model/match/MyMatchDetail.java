package com.jth.wematch.model.match;

import com.jth.wematch.entity.Match;
import com.jth.wematch.entity.MatchApply;
import com.jth.wematch.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyMatchDetail {
    @ApiModelProperty(notes = "게시글 제목", required = true)
    @NotNull
    private String title;
    @ApiModelProperty(notes = "경기 장소 및 상세 주소", required = true)
    @NotNull
    private String matchPlace;
    @ApiModelProperty(notes = "게시글 내용", required = true)
    @NotNull
    private String contents;
    @ApiModelProperty(notes = "경기 날짜", required = true)
    @NotNull
    private LocalDate sportDate;

    private MyMatchDetail(MyMatchDetailBuilder builder) {
        this.title = builder.title;
        this.matchPlace = builder.matchPlace;
        this.contents = builder.contents;
        this.sportDate = builder.sportDate;
    }

    public static class MyMatchDetailBuilder implements CommonModelBuilder<MyMatchDetail> {
        private final String title;
        private final String matchPlace;
        private final String contents;
        private final LocalDate sportDate;

        public MyMatchDetailBuilder(MatchApply matchApply) {
            this.title = matchApply.getMatch().getTitle();
            this.matchPlace = matchApply.getMatch().getPlace() + "(" + matchApply.getMatch().getPlaceAddress() + ")";
            this.contents = matchApply.getMatch().getContents();
            this.sportDate = matchApply.getMatch().getSportDate();
        }

        @Override
        public MyMatchDetail build() {
            return new MyMatchDetail();
        }
    }
}
