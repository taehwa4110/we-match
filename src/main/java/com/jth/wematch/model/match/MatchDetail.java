package com.jth.wematch.model.match;

import com.jth.wematch.entity.Match;
import com.jth.wematch.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MatchDetail {
    @ApiModelProperty(notes = "게시글 제목", required = true)
    @NotNull
    private String title;
    @ApiModelProperty(notes = "경기 장소 및 상세 주소", required = true)
    @NotNull
    private String matchPlace;
    @ApiModelProperty(notes = "게시글 내용", required = true)
    @NotNull
    private String contents;
    @ApiModelProperty(notes = "경기 날짜", required = true)
    @NotNull
    private LocalDate sportDate;
    @ApiModelProperty(notes = "현재 인원 수", required = true)
    @NotNull
    private Integer peopleCount;

    private MatchDetail(MatchDetailBuilder builder) {
        this.title = builder.title;
        this.matchPlace = builder.matchPlace;
        this.contents = builder.contents;
        this.sportDate = builder.sportDate;
        this.peopleCount = builder.peopleCount;
    }

    public static class MatchDetailBuilder implements CommonModelBuilder<MatchDetail> {
        private final String title;
        private final String matchPlace;
        private final String contents;
        private final LocalDate sportDate;
        private final Integer peopleCount;

        public MatchDetailBuilder(Match match) {
            this.title = match.getTitle();
            this.matchPlace = match.getPlace() + "(" + match.getPlaceAddress() + ")";
            this.contents = match.getContents();
            this.sportDate = match.getSportDate();
            this.peopleCount = match.getPeopleCount();
        }

        @Override
        public MatchDetail build() {
            return new MatchDetail(this);
        }
    }
}
