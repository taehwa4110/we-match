package com.jth.wematch.model.match;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MatchInfoUpdateRequest {
    @ApiModelProperty(notes = "경기 장소 (2 ~ 30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String place;

    @ApiModelProperty(notes = "경기 장소 상세 주소(2 ~ 40자)", required = true)
    @NotNull
    @Length(min = 2, max = 40)
    private String placeAddress;

    @ApiModelProperty(notes = "게시글 제목 (2 ~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String title;

    @ApiModelProperty(notes = "게시글 내용 ( ~ 255자)", required = true)
    @NotNull
    @Length(max = 255)
    private String contents;

    @ApiModelProperty(notes = "경기일", required = true )
    @NotNull
    private LocalDate sportDate;
}
