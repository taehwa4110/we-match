package com.jth.wematch.model.match;

import com.jth.wematch.entity.MatchApply;
import com.jth.wematch.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MatchApplyDetail {
    @ApiModelProperty(notes = "고객 이름")
    private String memberName;
    @ApiModelProperty(notes = "고객 성별")
    private String gender;
    @ApiModelProperty(notes = "고객 전화번호")
    private String phone;
    @ApiModelProperty(notes = "고객 주종목")
    private String sportsType;
    @ApiModelProperty(notes = "고객 활동지")
    private String location;

    private MatchApplyDetail(MatchApplyDetailBuilder builder) {
        this.memberName = builder.memberName;
        this.gender = builder.gender;
        this.phone = builder.phone;
        this.sportsType = builder.sportsType;
        this.location = builder.location;
    }

    public static class MatchApplyDetailBuilder implements CommonModelBuilder<MatchApplyDetail> {
        private final String memberName;
        private final String gender;
        private final String phone;
        private final String sportsType;
        private final String location;

        public MatchApplyDetailBuilder(MatchApply matchApply) {
            this.memberName = matchApply.getMember().getName();
            this.gender = matchApply.getMember().getGender().getName();
            this.phone = matchApply.getMember().getPhone();
            this.sportsType = matchApply.getMember().getSportsType() == null ? "주 종목 없음" : matchApply.getMember().getSportsType();
            this.location = matchApply.getMember().getLocation() == null ? "활동지 설정 안함" : matchApply.getMember().getLocation();
        }

        @Override
        public MatchApplyDetail build() {
            return new MatchApplyDetail(this);
        }
    }
}
