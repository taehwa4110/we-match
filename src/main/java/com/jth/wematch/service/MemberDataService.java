package com.jth.wematch.service;

import com.jth.wematch.entity.Member;
import com.jth.wematch.enums.Gender;
import com.jth.wematch.enums.MemberGroup;
import com.jth.wematch.exception.*;
import com.jth.wematch.lib.CommonCheck;
import com.jth.wematch.model.common.ListResult;
import com.jth.wematch.model.member.MemberInfoUpdateRequest;
import com.jth.wematch.model.member.MemberJoinRequest;
import com.jth.wematch.model.member.MemberListItem;
import com.jth.wematch.model.member.MemberMyInfoDetail;
import com.jth.wematch.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;

    public void setFirstMember() {
        String username = "wjdxoghk1";
        String password = "taehwa4110";
        LocalDate birthday = LocalDate.parse("1998-07-29");
        Gender gender = Gender.MAN;
        String phone = "010-8547-4110";

        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            MemberJoinRequest joinRequest = new MemberJoinRequest();
            joinRequest.setUsername(username);
            joinRequest.setPassword(password);
            joinRequest.setPasswordRe(password);
            joinRequest.setName("최고관리자");
            joinRequest.setBirthday(birthday);
            joinRequest.setGender(gender);
            joinRequest.setPhone(phone);
            setMember(MemberGroup.ROLE_ADMIN, joinRequest);
        }
    }

    public void setMember(MemberGroup memberGroup, MemberJoinRequest joinRequest) {
        if (!CommonCheck.checkUsername(joinRequest.getUsername())) throw new CNoCorrectUsernameException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!joinRequest.getPassword().equals(joinRequest.getPasswordRe())) throw new CIncorrectPasswordException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(joinRequest.getUsername())) throw new CDuplicationUsernameException(); // 중복된 아이디가 존재합니다 던지기

        joinRequest.setPassword(passwordEncoder.encode(joinRequest.getPassword()));

        Member member = new Member.MemberBuilder(memberGroup, joinRequest).build();
        memberRepository.save(member);
    }

    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }

    public Member getMemberData(long memberId) {
        return memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
    }

    public MemberMyInfoDetail getMyInfo(long memberId) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        return new MemberMyInfoDetail.MemberMyInfoDetailBuilder(member).build();
    }

    public ListResult<MemberListItem> getMemberList() {
        List<MemberListItem> result = new LinkedList<>();

        List<Member> members = memberRepository.findAll();

        members.forEach(member -> {
            MemberListItem memberListItem = new MemberListItem.MemberListItemBuilder(member).build();
            result.add(memberListItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putMemberInfo(long memberId, MemberInfoUpdateRequest request) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        member.putMemberInfo(request);

        memberRepository.save(member);
    }

    public void delMember(long memberId) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        if (!member.isEnabled())throw new CNoMemberDataException();

        member.putIsEnabled();
        memberRepository.save(member);
    }
}
