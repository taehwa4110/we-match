package com.jth.wematch.service;

import com.jth.wematch.entity.Match;
import com.jth.wematch.entity.MatchApply;
import com.jth.wematch.entity.Member;
import com.jth.wematch.enums.AcceptStatus;
import com.jth.wematch.exception.CAlreadyApplyDataException;
import com.jth.wematch.exception.CMissingDataException;
import com.jth.wematch.exception.CNoMatchDataException;
import com.jth.wematch.exception.CSamePersonDataException;
import com.jth.wematch.model.common.ListResult;
import com.jth.wematch.model.match.*;
import com.jth.wematch.repository.MatchApplyRepository;
import com.jth.wematch.repository.MatchRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MatchService {
    private final MatchRepository matchRepository;
    private final MatchApplyRepository matchApplyRepository;

    public void setMatch(Member member, MatchRegisterRequest request) {
        Match match = new Match.MatchBuilder(member, request).build();
        matchRepository.save(match);
    }

    public MatchDetail getMatchDetail(long matchId) {
        Match match = matchRepository.findById(matchId).orElseThrow(CMissingDataException::new);
        return new MatchDetail.MatchDetailBuilder(match).build();
    }

    public ListResult<MatchListItem> getMatchList() {
        List<MatchListItem> result = new LinkedList<>();

        List<Match> matches = matchRepository.findAll();

        matches.forEach(match -> {
            MatchListItem matchListItem = new MatchListItem.MatchListItemBuilder(match).build();
            result.add(matchListItem);
        });

        return ListConvertService.settingResult(result);
    }

    public ListResult<MatchListItem> getMatchList(String place, LocalDate sportDate) {
        List<MatchListItem> result = new LinkedList<>();

        List<Match> matches = matchRepository.findAllByPlaceOrSportDateOrderByIdAsc(place, sportDate);

        matches.forEach(match -> {
            MatchListItem matchListItem = new MatchListItem.MatchListItemBuilder(match).build();
            result.add(matchListItem);
        });

        return ListConvertService.settingResult(result);
    }


    public ListResult<MatchListItem> getMyMatchList(long memberId) {
        List<MatchListItem> result = new LinkedList<>();

        List<Match> matches = matchRepository.findAllByMember_Id(memberId);

        matches.forEach(match -> {
            MatchListItem matchListItem = new MatchListItem.MatchListItemBuilder(match).build();
            result.add(matchListItem);
        });

        return ListConvertService.settingResult(result);
    }

    public MatchDetail getMyMatchDetail(long memberId, long matchId) {
        Match match = matchRepository.findAllByMember_IdAndId(memberId, matchId);
        return new MatchDetail.MatchDetailBuilder(match).build();
    }

    public void putMatchInfo(long matchId, MatchInfoUpdateRequest request) {
        Match match = matchRepository.findById(matchId).orElseThrow(CMissingDataException::new);
        match.putMatchInfo(request);

        matchRepository.save(match);
    }

    public void putMatchEnable(long matchId) {
        Match match = matchRepository.findById(matchId).orElseThrow(CMissingDataException::new);

        if (!match.getIsEnable())throw new CNoMatchDataException();

        match.putMatchEnable();
        matchRepository.save(match);
    }

    public void setMatchApply(Member member, long matchId) {
        Match match = matchRepository.findById(matchId).orElseThrow(CMissingDataException::new);

        if (!match.getIsEnable())throw new CNoMatchDataException();
        if (member.equals(match.getMember())) throw new CSamePersonDataException();

        Optional<MatchApply> matchApplies = matchApplyRepository.findByMemberAndMatch_IdAndMatch_IsEnable(member, matchId, true);
        if (matchApplies.isPresent())throw new CAlreadyApplyDataException();

        MatchApply matchApply = new MatchApply.MatchApplyBuilder(member, match).build();
        matchApplyRepository.save(matchApply);
    }

    public ListResult<MatchApplyListItem> getMatchApply(long matchId, int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        Page<MatchApply> matchApplies = matchApplyRepository.findAllByMatch_IdAndMatch_IsEnable(matchId, true, pageRequest);

        List<MatchApplyListItem> result = new LinkedList<>();

        matchApplies.forEach(matchApply -> {
            MatchApplyListItem matchApplyListItem = new MatchApplyListItem.MatchApplyListItemBuilder(matchApply).build();
            result.add(matchApplyListItem);
        });

        return ListConvertService.settingResult(result, matchApplies.getTotalElements(), matchApplies.getTotalPages(), matchApplies.getNumber());
    }

    public MatchApplyDetail getMatchApplyDetail(long matchApplyId) {
        MatchApply matchApply = matchApplyRepository.findById(matchApplyId).orElseThrow(CMissingDataException::new);
        return new MatchApplyDetail.MatchApplyDetailBuilder(matchApply).build();
    }

    public ListResult<MatchApplyListItem> getMyMatchApply(Member member, int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);
        Page<MatchApply> matchApplies = matchApplyRepository.findAllByMemberAndMatch_IsEnable(member, true, pageRequest);

        List<MatchApplyListItem> result = new LinkedList<>();

        matchApplies.forEach(matchApply -> {
            MatchApplyListItem matchApplyListItem = new MatchApplyListItem.MatchApplyListItemBuilder(matchApply).build();
            result.add(matchApplyListItem);
        });

        return ListConvertService.settingResult(result, matchApplies.getTotalElements(), matchApplies.getTotalPages(), matchApplies.getNumber());
    }

    public MyMatchDetail getMyMatchApplyDetail(long matchApplyId) {
        MatchApply matchApply = matchApplyRepository.findById(matchApplyId).orElseThrow(CMissingDataException::new);
        return new MyMatchDetail.MyMatchDetailBuilder(matchApply).build();
    }

    public void putMatchAcceptStatus(long matchApplyId, AcceptStatus acceptStatus) {
        MatchApply matchApply = matchApplyRepository.findById(matchApplyId).orElseThrow(CMissingDataException::new);
        if (!matchApply.getMatch().getIsEnable())throw new CNoMatchDataException();

        matchApply.putAcceptStatus(acceptStatus);
        matchApplyRepository.save(matchApply);
    }

    public void delMatchApply(long matchApplyId) {
        MatchApply matchApply = matchApplyRepository.findById(matchApplyId).orElseThrow(CMissingDataException::new);
        if (!matchApply.getIsEnabled())throw new CMissingDataException();

        matchApply.putIsEnabled();
        matchApplyRepository.save(matchApply);
    }
}
