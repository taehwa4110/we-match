package com.jth.wematch.service;

import com.jth.wematch.entity.Member;
import com.jth.wematch.exception.CUsernameSignFailedException;
import com.jth.wematch.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService { // 이 서비스를 통해서 검사 시킬 것입니다// 유저를 디테일하게 검하는 기능
    private final MemberRepository memberRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException { //로그인하는데 회원가입이 안된 아이인거야 그러면 우리만의 메세지를 띄어주어야합니다.
        Member member = memberRepository.findByUsername(username).orElseThrow(CUsernameSignFailedException::new); //데이터들 갖고온다
        return member;
    }
}
