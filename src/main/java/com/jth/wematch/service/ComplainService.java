package com.jth.wematch.service;

import com.jth.wematch.entity.Complain;
import com.jth.wematch.entity.ComplainAnswer;
import com.jth.wematch.entity.Member;
import com.jth.wematch.enums.ProcessStatus;
import com.jth.wematch.exception.CAlreadyComplainAnswerDataException;
import com.jth.wematch.exception.CMissingDataException;
import com.jth.wematch.model.common.ListResult;
import com.jth.wematch.model.complain.ComplainAnswerRequest;
import com.jth.wematch.model.complain.ComplainDetail;
import com.jth.wematch.model.complain.ComplainListItem;
import com.jth.wematch.model.complain.ComplainRegisterRequest;
import com.jth.wematch.repository.ComplainAnswerRepository;
import com.jth.wematch.repository.ComplainRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ComplainService {
    private final ComplainRepository complainRepository;
    private final ComplainAnswerRepository complainAnswerRepository;

    public void setComplain(Member member, ComplainRegisterRequest request) {
        Complain complain = new Complain.ComplainBuilder(member, request).build();
        complainRepository.save(complain);
    }

    public ComplainDetail getComplainDetail(long complainId) {
        Complain complain = complainRepository.findById(complainId).orElseThrow(CMissingDataException::new);
        return new ComplainDetail.ComplainDetailBuilder(complain).build();
    }

    public ListResult<ComplainListItem> getComplainList(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);
        Page<Complain> complains = complainRepository.findAllByMember_IsEnabledOrderByIdAsc(true, pageRequest);

        List<ComplainListItem> result = new LinkedList<>();

        complains.forEach(complain -> {
            ComplainListItem complainListItem = new ComplainListItem.ComplainListItemBuilder(complain).build();
            result.add(complainListItem);
        });

        return ListConvertService.settingResult(result, complains.getTotalElements(), complains.getTotalPages(), complains.getNumber());
    }

    public ListResult<ComplainListItem> getMyComplainList(long memberId, int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);
        Page<Complain> complains = complainRepository.findAllByMember_IdOrderByIdAsc(memberId, pageRequest);

        List<ComplainListItem> result = new LinkedList<>();

        complains.forEach(complain -> {
            ComplainListItem complainListItem = new ComplainListItem.ComplainListItemBuilder(complain).build();
            result.add(complainListItem);
        });

        return ListConvertService.settingResult(result, complains.getTotalElements(), complains.getTotalPages(), complains.getNumber());
    }

    public void setComplainAnswer(Member member, long complainId, ComplainAnswerRequest request) {
        Complain complain = complainRepository.findById(complainId).orElseThrow(CMissingDataException::new);
        if (complain.getProcessStatus().equals(ProcessStatus.ANSWER_COMPLETE))throw new CAlreadyComplainAnswerDataException();

        ComplainAnswer complainAnswer = new ComplainAnswer.ComplainAnswerBuilder(member, complain, request).build();
        complain.putProcessStatus();

        complainAnswerRepository.save(complainAnswer);
        complainRepository.save(complain);
    }

}
