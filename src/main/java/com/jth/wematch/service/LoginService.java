package com.jth.wematch.service;

import com.jth.wematch.configure.JwtTokenProvider;
import com.jth.wematch.entity.Member;
import com.jth.wematch.enums.MemberGroup;
import com.jth.wematch.exception.CMissingDataException;
import com.jth.wematch.exception.CNoMemberDataException;
import com.jth.wematch.model.member.LoginRequest;
import com.jth.wematch.model.member.LoginResponse;
import com.jth.wematch.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;

    // 로그인 타입은 WEB or APP (WEB인경우 토큰 유효시간 10시간, APP은 1년)
    public LoginResponse doLogin(MemberGroup memberGroup, LoginRequest loginRequest, String loginType) {
        Member member = memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기

        if (!member.getIsEnabled()) throw new CNoMemberDataException(); // 회원탈퇴인 경우인데 DB에 남아있으므로 이걸 안 들키려면 회원정보가 없습니다 이걸로 던져야 함.
        if (!member.getMemberGroup().equals(memberGroup)) throw new CNoMemberDataException(); // 일반회원이 최고관리자용으로 로그인하려거나 이런 경우이므로 메세지는 회원정보가 없습니다로 일단 던짐.
        if (!passwordEncoder.matches(loginRequest.getPassword(), member.getPassword())) throw new CMissingDataException(); // 비밀번호가 일치하지 않습니다 던짐 // 비밀번호 일치하지않다고 예외를 생성햇지만 정확히 뭐가 틀렷는지 말하면 보안에 위험이 있기에 회원정보가 없습니다로 던지겠습니다.

        String token = jwtTokenProvider.createToken(String.valueOf(member.getUsername()), member.getMemberGroup().toString(), loginType);

        return new LoginResponse.LoginResponseBuilder(token, member.getName()).build();
    }
}
