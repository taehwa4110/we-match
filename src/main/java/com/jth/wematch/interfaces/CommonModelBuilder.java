package com.jth.wematch.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
