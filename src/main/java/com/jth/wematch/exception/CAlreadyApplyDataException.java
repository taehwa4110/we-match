package com.jth.wematch.exception;

public class CAlreadyApplyDataException extends RuntimeException{
    public CAlreadyApplyDataException(String msg, Throwable t) {
        super(msg, t);
    }

    public CAlreadyApplyDataException(String msg) {
        super(msg);
    }

    public CAlreadyApplyDataException() {
        super();
    }
}
