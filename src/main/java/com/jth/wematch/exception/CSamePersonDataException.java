package com.jth.wematch.exception;

public class CSamePersonDataException extends RuntimeException{
    public CSamePersonDataException(String msg, Throwable t) {
        super(msg, t);
    }

    public CSamePersonDataException(String msg) {
        super(msg);
    }

    public CSamePersonDataException() {
        super();
    }
}
