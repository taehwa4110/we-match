package com.jth.wematch.exception;

public class CAlreadyComplainAnswerDataException extends RuntimeException{
    public CAlreadyComplainAnswerDataException(String msg, Throwable t) {
        super(msg, t);
    }

    public CAlreadyComplainAnswerDataException(String msg) {
        super(msg);
    }

    public CAlreadyComplainAnswerDataException() {
        super();
    }
}
