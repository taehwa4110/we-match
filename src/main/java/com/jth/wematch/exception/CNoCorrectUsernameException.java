package com.jth.wematch.exception;

public class CNoCorrectUsernameException extends RuntimeException{
    public CNoCorrectUsernameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoCorrectUsernameException(String msg) {
        super(msg);
    }

    public CNoCorrectUsernameException() {
        super();
    }
}
