package com.jth.wematch.exception;

public class CDuplicationUsernameException extends RuntimeException{
    public CDuplicationUsernameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDuplicationUsernameException(String msg) {
        super(msg);
    }

    public CDuplicationUsernameException() {
        super();
    }
}
