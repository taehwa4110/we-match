package com.jth.wematch.exception;

public class CUsernameSignFailedException extends RuntimeException{
    public CUsernameSignFailedException(String msg, Throwable t) {
        super(msg, t);
    }

    public CUsernameSignFailedException(String msg) {
        super(msg);
    }

    public CUsernameSignFailedException() {
        super();
    }
}
