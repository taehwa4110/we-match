package com.jth.wematch.exception;

public class CNoMatchDataException extends RuntimeException{
    public CNoMatchDataException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoMatchDataException(String msg) {
        super(msg);
    }

    public CNoMatchDataException() {
        super();
    }
}
