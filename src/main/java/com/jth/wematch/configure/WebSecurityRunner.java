package com.jth.wematch.configure;

import com.jth.wematch.service.MemberDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class WebSecurityRunner implements ApplicationRunner { //어플리케이션이 시작하면서 같이 실행시켜주세여
    private final MemberDataService memberDataService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        memberDataService.setFirstMember();
    }
}
