package com.jth.wematch.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final JwtTokenProvider jwtTokenProvider; // security와  jwt는 띠어낼수없어 같이 협업해야하는데 그중에 jwtTokenProvider과 협업함

    private static final String[] AUTH_WHITELIST = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };

    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring().antMatchers(AUTH_WHITELIST);
    }


    @Bean
    @Override //부모 기능에 포함되어있는데 또 치는거라 override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManagerBean(); //super는 부모 즉 extends로 갖고온 것을 말합니다.
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .authorizeRequests()
                        .antMatchers(HttpMethod.GET, "/exception/**").permitAll() // 전체 허용
                        .antMatchers("/v1/login/**").permitAll() // 전체허용
                        .antMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN")
                        .antMatchers("/v1/auth-test/test-user").hasAnyRole("USER")
                        .antMatchers("/v1/auth-test/test-all").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/auth-test/login-all/**").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/member/new/user").permitAll()
                        .antMatchers("/v1/member/new/admin").hasAnyRole("ADMIN")
                        .antMatchers("/v1/member/my-info/{memberId}").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/member/member-list").hasAnyRole("ADMIN")
                        .antMatchers("/v1/member/member-info/{memberId}").hasAnyRole("USER")
                        .antMatchers("/v1/member/delete/member/{memberId}").hasAnyRole("ADMIN")
                        .antMatchers("/v1/match/new/{memberId}").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/match/search").permitAll()
                        .antMatchers("/v1/match/my-match-list/{memberId}").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/match/match-detail/{matchId}").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/match/my-match-detail/{memberId}/{matchId}").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/match/my-match-info/{matchId}").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/match/match-day-off/{matchId}").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/match/match-apply/{memberId}/{matchId}").hasAnyRole( "USER")
                        .antMatchers("/v1/match/match-apply-list/page/{pageNum}/{matchId}").hasAnyRole( "USER")
                        .antMatchers("/v1/match/match-apply-detail/{matchApplyId}").hasAnyRole( "USER")
                        .antMatchers("/v1/match/my-match-apply-list/page/{pageNum}/{memberId}").hasAnyRole( "USER")
                        .antMatchers("/v1/match/my-match-apply-detail/{matchApplyId}").hasAnyRole( "USER")
                        .antMatchers("/v1/match/match-accept-status/{matchApplyId}/{acceptStatus}").hasAnyRole( "USER")
                        .antMatchers("/v1/match/match-apply/{matchApplyId}").hasAnyRole( "USER")
                        .antMatchers("/v1/complain/new/complain/{memberId}").hasAnyRole( "USER")
                        .antMatchers("/v1/complain/complain-list/page/{pageNum}").hasAnyRole( "ADMIN", "USER")
                        .antMatchers("/v1/complain/register/complain-answer/member/{memberId}/{complainId}").hasAnyRole( "ADMIN")
                        .antMatchers("/v1/complain/complain-detail/{complainId}").hasAnyRole( "ADMIN")
                        .antMatchers("/v1/complain/my-complain-list/member/{memberId}/page/{pageNum}").hasAnyRole( "ADMIN", "USER")
                        .anyRequest().hasRole("ADMIN") // 기본 접근 권한은 ROLE_ADMIN
                .and()
                    .exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler())
                .and()
                    .exceptionHandling().authenticationEntryPoint(new CustomAuthenticationEntryPoint())
                .and()
                    .addFilterBefore(new JwtAuthenticationFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source =
                new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOriginPattern("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

}
