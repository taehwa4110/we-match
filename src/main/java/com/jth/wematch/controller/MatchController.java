package com.jth.wematch.controller;

import com.jth.wematch.entity.Member;
import com.jth.wematch.enums.AcceptStatus;
import com.jth.wematch.model.common.CommonResult;
import com.jth.wematch.model.common.ListResult;
import com.jth.wematch.model.common.SingleResult;
import com.jth.wematch.model.match.*;
import com.jth.wematch.service.MatchService;
import com.jth.wematch.service.MemberDataService;
import com.jth.wematch.service.ResponseService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "게시글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/match")
public class MatchController {
    private final MatchService matchService;
    private final MemberDataService memberDataService;
    @ApiOperation(value = "게시글 등록")
    @PostMapping("/new/{memberId}")
    public CommonResult setMatch(@PathVariable long memberId, @RequestBody @Valid MatchRegisterRequest request) {
        Member member = memberDataService.getMemberData(memberId);
        matchService.setMatch(member, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시글 상세정보 가져오기")
    @GetMapping("/match-detail/{matchId}")
    public SingleResult<MatchDetail> getMatchDetail(@PathVariable long matchId) {
        return ResponseService.getSingleResult(matchService.getMatchDetail(matchId));
    }

    @ApiOperation(value = "게시글 리스트 가져오기")
    @GetMapping("/search")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "place", value = "경기 장소", required = false),
            @ApiImplicitParam(name = "sportDate", value = "경기 날짜", required = false)
    })
    // TODO: 2022-11-18 장소만 입력햇을때는 나오는데 경기날짜만 입력하면 리스트가 나오지않음. 조치 필요
    public ListResult<MatchListItem> getMatchList(
            @RequestParam(value = "place", required = false)String place,
            @RequestParam(value = "sportDate", required = false)LocalDate sportDate
            ) {
        if (place == null && sportDate == null) return ResponseService.getListResult(matchService.getMatchList(), true);
        else return ResponseService.getListResult(matchService.getMatchList(place, sportDate), true);
    }

    @ApiOperation(value = "내가 등록한 게시글 리스트 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "사원 시퀀스", required = true)
    })
    @GetMapping("/my-match-list/{memberId}")
    public ListResult<MatchListItem> getMyMatchList(@PathVariable long memberId) {
        return ResponseService.getListResult(matchService.getMyMatchList(memberId), true);
    }

    @ApiOperation(value = "내가 등록한 게시글 상세정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "고객 시퀀스", required = true),
            @ApiImplicitParam(name = "matchId", value = "경기 시퀀스", required = true)
    })
    @GetMapping("/my-match-detail/{memberId}/{matchId}")
    public SingleResult<MatchDetail> getMyMatchDetail(@PathVariable long memberId, @PathVariable long matchId) {
        return ResponseService.getSingleResult(matchService.getMyMatchDetail(memberId, matchId));
    }

    @ApiOperation(value = "게시글 내용 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchId", value = "경기 시퀀스", required = true)
    })
    @PutMapping("/my-match-info/{matchId}")
    public CommonResult putMatch(@PathVariable long matchId, @RequestBody @Valid MatchInfoUpdateRequest request) {
        matchService.putMatchInfo(matchId, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "경기 게시글 삭제하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchId", value = "경기 시퀀스", required = true)
    })
    @DeleteMapping("/match-day-off/{matchId}")
    public CommonResult delMatch(@PathVariable long matchId) {
        matchService.putMatchEnable(matchId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "경기 참가 신청 등록하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "고객 시퀀스", required = true),
            @ApiImplicitParam(name = "matchId", value = "경기 시퀀스", required = true)
    })
    @PostMapping("/match-apply/{memberId}/{matchId}")
    public CommonResult setMatchApply(@PathVariable long memberId, @PathVariable long matchId) {
        Member member = memberDataService.getMemberData(memberId);
        matchService.setMatchApply(member, matchId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "경기 참가 신청 리스트 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "페이지 번호", required = true),
            @ApiImplicitParam(name = "matchId", value = "경기 시퀀스", required = true)
    })
    @GetMapping("/match-apply-list/page/{pageNum}/{matchId}")
    public ListResult<MatchApplyListItem> getMatchApplyList(@PathVariable int pageNum, @PathVariable long matchId) {
        return ResponseService.getListResult(matchService.getMatchApply(matchId, pageNum), true);
    }

    @ApiOperation(value = "경기 참가 신청한 인원 상세정보 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchApplyId", value = "경기 참가 신청 시퀀스", required = true)
    })
    @GetMapping("/match-apply-detail/{matchApplyId}")
    public SingleResult<MatchApplyDetail> getMatchApplyDetail(@PathVariable long matchApplyId) {
        return ResponseService.getSingleResult(matchService.getMatchApplyDetail(matchApplyId));
    }

    @ApiOperation(value = "내가 신청한 경기 리스트 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "페이지 번호", required = true),
            @ApiImplicitParam(name = "memberId", value = "고객 시퀀스", required = true)
    })
    @GetMapping("/my-match-apply-list/page/{pageNum}/{memberId}")
    public ListResult<MatchApplyListItem> getMyMatchApplyList(@PathVariable int pageNum, @PathVariable long memberId) {
        Member member = memberDataService.getMemberData(memberId);
        return ResponseService.getListResult(matchService.getMyMatchApply(member, pageNum), true);
    }

    @ApiOperation(value = "내가 신청한 경기 상세정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchApplyId", value = "경기 참가 신청 시퀀스", required = true)
    })
    //TODO return값이 null로 나오는 문제 발생 조치 요망
    @GetMapping("/my-match-apply-detail/{matchApplyId}")
    public SingleResult<MyMatchDetail> getMyMatchApplyDetail(@PathVariable long matchApplyId) {
        return ResponseService.getSingleResult(matchService.getMyMatchApplyDetail(matchApplyId));
    }

    @ApiOperation(value = "경기 참가 수락 여부 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchApplyId", value = "경기 참가 신청 시퀀스", required = true),
            @ApiImplicitParam(name = "acceptStatus", value = "경기 참가 수락 여부", required = true)
    })
    @PutMapping("/match-accept-status/{matchApplyId}/{acceptStatus}")
    public CommonResult putMatchAcceptStatus(@PathVariable long matchApplyId, @PathVariable AcceptStatus acceptStatus) {
        matchService.putMatchAcceptStatus(matchApplyId, acceptStatus);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "경기 참가 신청 삭제하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matchApplyId", value = "경기 참가 시퀀스", required = true)
    })
    @DeleteMapping("/match-apply/{matchApplyId}")
    public CommonResult delMatchApply(@PathVariable long matchApplyId) {
        matchService.delMatchApply(matchApplyId);

        return ResponseService.getSuccessResult();
    }
}
