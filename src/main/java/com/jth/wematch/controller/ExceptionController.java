package com.jth.wematch.controller;

import com.jth.wematch.exception.CAccessDeniedException;
import com.jth.wematch.exception.CAuthenticationEntryPointException;
import com.jth.wematch.model.common.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CAccessDeniedException(); // 이 주소로 들어오자마자 밖으로 던저버려
    }

    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CAuthenticationEntryPointException(); // 이 주소로 들어오자마자 던져라
    }
}
