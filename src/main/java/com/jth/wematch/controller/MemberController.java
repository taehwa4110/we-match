package com.jth.wematch.controller;

import com.jth.wematch.enums.MemberGroup;
import com.jth.wematch.model.common.CommonResult;
import com.jth.wematch.model.common.ListResult;
import com.jth.wematch.model.common.SingleResult;
import com.jth.wematch.model.member.*;
import com.jth.wematch.service.MemberDataService;
import com.jth.wematch.service.ResponseService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import com.jth.wematch.model.member.MemberJoinRequest;
import com.jth.wematch.service.MemberDataService;
import com.jth.wematch.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "사원 관리 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("v1//member")
public class MemberController {
    private final MemberDataService memberDataService;

    @ApiOperation(value = "일반 사용자 등록")
    @PostMapping("/new/user")
    public CommonResult setMemberUser(@RequestBody @Valid MemberJoinRequest request) {
        memberDataService.setMember(MemberGroup.ROLE_USER, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "관리자 등록")
    @PostMapping("/new/admin")
    public CommonResult setMemberAdmin(@RequestBody @Valid MemberJoinRequest request) {
        memberDataService.setMember(MemberGroup.ROLE_ADMIN, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "내 정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "고객 시퀀스", required = true)
    })
    @GetMapping("/my-info/{memberId}")
    public SingleResult<MemberMyInfoDetail> getMyInfo(@PathVariable long memberId) {
        return ResponseService.getSingleResult(memberDataService.getMyInfo(memberId));
    }

    @ApiOperation(value = "고객 리스트 가져오기")
    @GetMapping("/member-list")
    public ListResult<MemberListItem> getMemberList() {
        return ResponseService.getListResult(memberDataService.getMemberList(), true);
    }

    @ApiOperation(value = "고객 정보 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "사원 시퀀스", required = true)
    })
    @PutMapping("/member-info/{memberId}")
    public CommonResult getMemberInfo(@PathVariable long memberId, @RequestBody @Valid MemberInfoUpdateRequest request) {
        memberDataService.putMemberInfo(memberId, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "고객 삭제하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "고객 시퀀스", required = true)
    })
    @DeleteMapping("/delete/member/{memberId}")
    public CommonResult delMember(@PathVariable long memberId) {
        memberDataService.delMember(memberId);

        return ResponseService.getSuccessResult();
    }


}
