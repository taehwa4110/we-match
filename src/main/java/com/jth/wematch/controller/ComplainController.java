package com.jth.wematch.controller;

import com.jth.wematch.entity.Complain;
import com.jth.wematch.entity.Member;
import com.jth.wematch.model.common.CommonResult;
import com.jth.wematch.model.common.ListResult;
import com.jth.wematch.model.common.SingleResult;
import com.jth.wematch.model.complain.ComplainAnswerRequest;
import com.jth.wematch.model.complain.ComplainDetail;
import com.jth.wematch.model.complain.ComplainListItem;
import com.jth.wematch.model.complain.ComplainRegisterRequest;
import com.jth.wematch.service.ComplainService;
import com.jth.wematch.service.MemberDataService;
import com.jth.wematch.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "문의사항 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/complain")
public class ComplainController {
    private final ComplainService complainService;
    private final MemberDataService memberDataService;

    @ApiOperation(value = "문의 등록하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "고객 시퀀스", required = true)
    })
    @PostMapping("/new/complain/{memberId}")
    public CommonResult setComplain(@PathVariable long memberId, @RequestBody @Valid ComplainRegisterRequest request) {
        Member member = memberDataService.getMemberData(memberId);
        complainService.setComplain(member, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "문의 사항 상세정보 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complainId", required = true)
    })
    @GetMapping("/complain-detail/{complainId}")
    public SingleResult<ComplainDetail> getComplainDetail(@PathVariable long complainId) {
        return ResponseService.getSingleResult(complainService.getComplainDetail(complainId));
    }

    @ApiOperation(value = "문의 리스트 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "페이지 번호", required = true)
    })
    @GetMapping("/complain-list/page/{pageNum}")
    public ListResult<ComplainListItem> getComplainList(@PathVariable int pageNum) {
        return ResponseService.getListResult(complainService.getComplainList(pageNum), true);
    }

    @ApiOperation(value = "내가 문의한 리스트 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "고객 시퀀스", required = true),
            @ApiImplicitParam(name = "pageNum", value = "페이지 번호", required = true)
    })
    @GetMapping("/my-complain-list/member/{memberId}/page/{pageNum}")
    public ListResult<ComplainListItem> getMyComplainList(@PathVariable long memberId, @PathVariable int pageNum) {
        return ResponseService.getListResult(complainService.getMyComplainList(memberId, pageNum), true);
    }

    @ApiOperation(value = "문의 사항 답변 등록하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "고객 시퀀스", required = true),
            @ApiImplicitParam(name = "complainId", value = "문의 사항 시퀀스", required = true)
    })
    @PostMapping("/register/complain-answer/member/{memberId}/{complainId}")
    public CommonResult setComplainAnswer(@PathVariable long memberId, @PathVariable long complainId, @RequestBody @Valid ComplainAnswerRequest request) {
        Member member = memberDataService.getMemberData(memberId);
        complainService.setComplainAnswer(member, complainId, request);

        return ResponseService.getSuccessResult();
    }
}
