package com.jth.wematch.advice;

import com.jth.wematch.enums.ResultCode;
import com.jth.wematch.exception.*;
import com.jth.wematch.model.common.CommonResult;
import com.jth.wematch.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CAccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAccessDeniedException e) {
        return ResponseService.getFailResult(ResultCode.ACCESS_DENIED);
    }

    @ExceptionHandler(CUsernameSignFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUsernameSignFailedException e) {
        return ResponseService.getFailResult(ResultCode.USERNAME_SIGN_IN_FAILED);
    }

    @ExceptionHandler(CAuthenticationEntryPointException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAuthenticationEntryPointException e) {
        return ResponseService.getFailResult(ResultCode.AUTHENTICATION_ENTRY_POINT);
    }

    @ExceptionHandler(CNoCorrectUsernameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoCorrectUsernameException e) {
        return ResponseService.getFailResult(ResultCode.NO_CORRECT_USERNAME);
    }

    @ExceptionHandler(CDuplicationUsernameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDuplicationUsernameException e) {
        return ResponseService.getFailResult(ResultCode.DUPLICATION_USERNAME);
    }

    @ExceptionHandler(CIncorrectPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CIncorrectPasswordException e) {
        return ResponseService.getFailResult(ResultCode.DUPLICATION_USERNAME);
    }

    @ExceptionHandler(CNoMemberDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoMemberDataException e) {
        return ResponseService.getFailResult(ResultCode.DUPLICATION_USERNAME);
    }

    @ExceptionHandler(CNoMatchDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoMatchDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_MATCH_DATA);
    }

    @ExceptionHandler(CAlreadyApplyDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CAlreadyApplyDataException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_APPLY_DATA);
    }

    @ExceptionHandler(CSamePersonDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CSamePersonDataException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_APPLY_DATA);
    }

    @ExceptionHandler(CAlreadyComplainAnswerDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CAlreadyComplainAnswerDataException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_APPLY_DATA);
    }
}
