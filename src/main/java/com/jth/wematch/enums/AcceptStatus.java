package com.jth.wematch.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AcceptStatus {
    ACCEPT("수락"),
    WAITING("대기중"),
    REFUSAL("거절");

    private final String name;
}
