package com.jth.wematch.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0,"성공하였습니다.")
    ,FAILED(-1, "실패하였습니다.")

    ,ACCESS_DENIED(-1000, "권한이 없습니다.")
    ,USERNAME_SIGN_IN_FAILED(-1001, "가입된 사용자가 아닙니다.")
    ,AUTHENTICATION_ENTRY_POINT(-1002, "접근 권한이 없습니다.")
    ,NO_CORRECT_USERNAME(-1050, "유효한 아이디 형식이 아닙니다.")
    ,DUPLICATION_USERNAME(-1051, "중복된 아이디가 존재합니다.")
    ,INCORRECT_PASSWORD(-1051, "비밀번호가 일치하지않습니다.")
    ,NO_MEMBER_DATA(-1100, "멤버 데이터가 없습니다.")
    ,NO_MATCH_DATA(-1200, "경기 게시글 데이터가 없습니다.")
    ,ALREADY_APPLY_DATA(-1300, "이미 경기 게시글에 신청하셧습니다.")
    ,SAME_PERSON(-1400, "신청자와 게시글 작성자가 같습니다.")
    ,ALREADY_COMPLAIN_ANSWER(-1500, "이미 문의에 대한 답변이 입력되어있습니다.")

    ,MISSING_DATA(-10000,"데이터를 찾을 수 없습니다.");

    private final Integer code;
    private final String msg;
}
