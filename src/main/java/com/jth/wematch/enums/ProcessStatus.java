package com.jth.wematch.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProcessStatus {
    ANSWER_COMPLETE("답변 완료"),
    ANSWER_WAITING("답변 대기중");

    private final String name;
}
